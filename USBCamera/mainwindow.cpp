#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_cameraDriver(new CameraDriver()),
    m_threadCameraDriver(new QThread(this))
{
    ui->setupUi(this);
    ui->graphicsView->setScene(new QGraphicsScene(this));
    ui->graphicsView->scene()->addItem(&m_pixmap);

    m_cameraDriver->moveToThread(m_threadCameraDriver);
    connect(m_threadCameraDriver, &QThread::finished, m_cameraDriver, &VideoDriver::deleteLater);
    m_threadCameraDriver->start();

    connect(this, &MainWindow::cameraStart, m_cameraDriver, &CameraDriver::on_start);
    connect(this, &MainWindow::cameraStop, m_cameraDriver, &CameraDriver::on_stop);
    connect(ui->pushButton_record, &QPushButton::clicked, m_cameraDriver, &CameraDriver::on_record);
    connect(ui->pushButton_snap, &QPushButton::clicked, m_cameraDriver, &CameraDriver::on_snap);
    connect(m_cameraDriver, &CameraDriver::frameReady, this, &MainWindow::on_frameView, Qt::QueuedConnection);
    emit cameraStart(0, 20.0, QStandardPaths::writableLocation(QStandardPaths::DesktopLocation));

}

MainWindow::~MainWindow()
{
    if (m_threadCameraDriver) {
        m_threadCameraDriver->quit();
        m_threadCameraDriver->wait();
    }
    delete ui;
}


void MainWindow::on_frameView(const cv::Mat &frame_cv)
{
    const QImage frame_qt(frame_cv.data,
                          frame_cv.cols,
                          frame_cv.rows,
                          static_cast<int>(frame_cv.step),
                          QImage::Format_RGB888);

    m_pixmap.setPixmap(QPixmap::fromImage(frame_qt.rgbSwapped()));
    ui->graphicsView->fitInView(&m_pixmap, Qt::KeepAspectRatio);
}
