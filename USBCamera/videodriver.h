#ifndef VIDEODRIVER_H
#define VIDEODRIVER_H

#include <QObject>
#include <QDir>
#include <QDateTime>

#include <opencv2/opencv.hpp>

class VideoDriver : public QObject
{
    Q_OBJECT

public:
    explicit VideoDriver(QObject *parent = nullptr);
    ~VideoDriver();

private:
    bool m_isSnapping;
    bool m_isRecording;
    cv::VideoWriter *m_writer;
    QString m_fileSnapshot;

public slots:
    void on_start(double fps, double frameWidth, double frameHeight, const QString &path);
    void on_stop();
    void on_snap(const QString &path);
    void on_frame(const cv::Mat &frame);
};

#endif // VIDEODRIVER_H
