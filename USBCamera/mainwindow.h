#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>

#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QDebug>

#include "cameradriver.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    CameraDriver *m_cameraDriver;
    QThread *m_threadCameraDriver;
    QGraphicsPixmapItem m_pixmap;

signals:
    void cameraStart(int cameraid, double frameRate, const QString &path);
    void cameraStop();

public slots:
    void on_frameView(const cv::Mat &frame_cv);
};

#endif // MAINWINDOW_H
