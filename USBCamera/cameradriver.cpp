#include "cameradriver.h"

CameraDriver::CameraDriver(QObject *parent) : QObject(parent),
    m_path(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)),
    m_videoCapture(nullptr),
    m_timerAcquire(new QTimer(this)),
    m_videoDriver(new VideoDriver()),
    m_threadVideoDriver(new QThread(this)),
    m_frameWidth(1.0),
    m_frameHeight(1.0)
{
    qRegisterMetaType<cv::Mat>();

    connect(m_timerAcquire, &QTimer::timeout, this, &CameraDriver::on_acquire);

    // alocate writer thread
    m_videoDriver->moveToThread(m_threadVideoDriver);
    connect(m_threadVideoDriver, &QThread::finished, m_videoDriver, &VideoDriver::deleteLater);
    m_threadVideoDriver->start();

    connect(this, &CameraDriver::startWriter, m_videoDriver, &VideoDriver::on_start);
    connect(this, &CameraDriver::stopWriter, m_videoDriver, &VideoDriver::on_stop);
    connect(this, &CameraDriver::snapFrame, m_videoDriver, &VideoDriver::on_snap);
    connect(this, &CameraDriver::frameReady, m_videoDriver, &VideoDriver::on_frame);

}


CameraDriver::~CameraDriver()
{
    if (m_timerAcquire->isActive())
        m_timerAcquire->stop();

    if (m_videoCapture != nullptr) {
        if (m_videoCapture->isOpened())
            m_videoCapture->release();
        delete m_videoCapture;
    }

    if (m_threadVideoDriver != nullptr) {
        emit stopWriter();
        m_threadVideoDriver->quit();
        m_threadVideoDriver->wait();
    }
}


void CameraDriver::on_start(int cameraid, double frameRateView, const QString &path)
{
    // open video capture
    m_videoCapture = new cv::VideoCapture(cameraid);
    if (!m_videoCapture->isOpened()) return;
    m_frameWidth = m_videoCapture->get(cv::CAP_PROP_FRAME_WIDTH);
    m_frameHeight = m_videoCapture->get(cv::CAP_PROP_FRAME_HEIGHT);
    m_frameRate = frameRateView;
    m_path = path;

    // start acquisition timer
    m_timerAcquire->start(static_cast<int>(1000.0/frameRateView));
}


void CameraDriver::on_stop()
{
    m_timerAcquire->stop();
}


void CameraDriver::on_acquire()
{
    cv::Mat frame_cv;

    // acquire frame
    if (!m_videoCapture->read(frame_cv)) {
        on_stop();
        return;
    }
    emit frameReady(frame_cv);
}


void CameraDriver::on_record(bool state)
{
    if (state) {
        emit startWriter(m_frameRate, m_frameWidth, m_frameHeight, m_path);
    }
    else {
        emit stopWriter();
    }
}


void CameraDriver::on_snap()
{
    emit snapFrame(m_path);
}
