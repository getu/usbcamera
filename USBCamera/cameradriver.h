#ifndef CAMERADRIVER_H
#define CAMERADRIVER_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include <QImage>
#include <QStandardPaths>
#include <QDebug>

#include <opencv2/opencv.hpp>

#include "videodriver.h"


class CameraDriver : public QObject
{
    Q_OBJECT

public:
    explicit CameraDriver(QObject *parent = nullptr);
    ~CameraDriver();

private:
    QString m_path;
    cv::VideoCapture *m_videoCapture;
    QTimer *m_timerAcquire;
    VideoDriver *m_videoDriver;
    QThread *m_threadVideoDriver;
    double m_frameWidth;
    double m_frameHeight;
    double m_frameRate;

private slots:
    void on_acquire();

signals:
    void frameReady(const cv::Mat &frame);
    void startWriter(double fps, double frameWidth, double frameHeight, const QString &path);
    void stopWriter();
    void snapFrame(const QString &path);

public slots:
    void on_start(int cameraid, double frameRateView, const QString &path);
    void on_stop();
    void on_record(bool state);
    void on_snap();

};

Q_DECLARE_METATYPE(cv::Mat)

#endif // CAMERADRIVER_H
