#include "videodriver.h"

VideoDriver::VideoDriver(QObject *parent) : QObject(parent),
    m_isSnapping(false),
    m_isRecording(false),
    m_writer(nullptr),
    m_fileSnapshot("test.jpg")
{

}


VideoDriver::~VideoDriver()
{
    if (m_writer != nullptr) {
        m_writer->release();
        delete m_writer;
    }
}


void VideoDriver::on_start(double fps, double frameWidth, double frameHeight, const QString &path)
{
    QString fileVideo = QDir(path).filePath("Video_" + QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss") + ".mkv");
    m_writer = new cv::VideoWriter(fileVideo.toStdString(), cv::VideoWriter::fourcc('X', '2', '6', '4'), fps, cv::Size(static_cast<int>(frameWidth), static_cast<int>(frameHeight)));
    //m_writer = new cv::VideoWriter(fileVideo.toStdString(), 0x21, fps, cv::Size(static_cast<int>(frameWidth), static_cast<int>(frameHeight)));
    m_isRecording = true;
}


void VideoDriver::on_stop()
{
    m_isRecording = false;
    if (m_writer != nullptr) m_writer->release();
}


void VideoDriver::on_snap(const QString &path)
{
    m_fileSnapshot = QDir(path).filePath("Snapshot_" + QDateTime::currentDateTime().toString("yyyyMMdd_hhmmss") + ".jpeg");
    m_isSnapping = true;
}


void VideoDriver::on_frame(const cv::Mat &frame)
{
    if (m_isRecording && (m_writer != nullptr)) {
        m_writer->write(frame);
    }

    if (m_isSnapping) {
        cv::imwrite(m_fileSnapshot.toStdString(), frame);
        m_isSnapping = false;
    }
}
